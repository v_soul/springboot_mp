package com.lqf.multitable.bean.crm;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author lqf
 * @since 2018-10-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class FyUser implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "user_id", type = IdType.AUTO)
    private Long userId;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 修改时间
     */
    private LocalDateTime updateTime;

    /**
     * 账号
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 姓名
     */
    private String realname;

    /**
     * 性别(0为女 1为男)
     */
    private Integer sex;

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 密码加密串
     */
    private String passwordMd5;

    /**
     * 开账号人
     */
    private Long parentId;

    /**
     * 是否禁用 0否 1是
     */
    private Integer status;

    /**
     * 授权大区
     */
    private String authArea;

    /**
     * 授权城市
     */
    private String authCity;

    private Integer role;

    /**
     * 用户剩余卡数
     */
    private Integer residueCardNumber;

    /**
     * 最后登录时间
     */
    private LocalDateTime lastLoginTime;

    /**
     * 最后登录ip地址
     */
    private String lastLoginIp;

    /**
     * 最后登录次数
     */
    private Integer lastLoginCount;

    /**
     * 渠道
     */
    private String authChannel;

    /**
     * 0 外网 1 内网
     */
    private Integer internet;

    /**
     * 金币
     */
    private Long goldCoin;


}
