package com.lqf.multitable.service.crm.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lqf.multitable.bean.crm.FyUser;
import com.lqf.multitable.bean.crm.UserRoleVo;
import com.lqf.multitable.dao.crm.FyUserMapper;
import com.lqf.multitable.service.crm.FyUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lqf
 * @since 2018-10-02
 */
@Service
public class FyUserServiceImpl extends ServiceImpl<FyUserMapper, FyUser> implements FyUserService {

    @Override
    public List<UserRoleVo> selectUserListPage(Page<UserRoleVo> page) {
        return this.baseMapper.selectUserListPage(page);
    }
}
