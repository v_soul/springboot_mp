package com.lqf.multitable.service.crm;

import com.lqf.multitable.bean.crm.FyRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lqf
 * @since 2018-10-02
 */
public interface FyRoleService extends IService<FyRole> {

}
