package com.lqf.multitable.service.crm;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lqf.multitable.bean.crm.FyUser;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lqf.multitable.bean.crm.UserRoleVo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lqf
 * @since 2018-10-02
 */
public interface FyUserService extends IService<FyUser> {


    List<UserRoleVo> selectUserListPage(Page<UserRoleVo> page);
}
