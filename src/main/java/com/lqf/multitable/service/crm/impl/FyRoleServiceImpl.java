package com.lqf.multitable.service.crm.impl;

import com.lqf.multitable.bean.crm.FyRole;
import com.lqf.multitable.dao.crm.FyRoleMapper;
import com.lqf.multitable.service.crm.FyRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lqf
 * @since 2018-10-02
 */
@Service
public class FyRoleServiceImpl extends ServiceImpl<FyRoleMapper, FyRole> implements FyRoleService {

}
