package com.lqf.multitable.dao.crm;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lqf.multitable.bean.crm.FyUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lqf.multitable.bean.crm.UserRoleVo;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lqf
 * @since 2018-10-02
 */
@Repository
public interface FyUserMapper extends BaseMapper<FyUser> {


    @Select("SELECT * FROM fy_user u LEFT JOIN fy_role r ON u.role = r.id")
    List<UserRoleVo> selectUserListPage(Page<UserRoleVo> pagination);

}
