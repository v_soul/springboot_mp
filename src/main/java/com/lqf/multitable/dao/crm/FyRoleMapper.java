package com.lqf.multitable.dao.crm;

import com.lqf.multitable.bean.crm.FyRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lqf
 * @since 2018-10-02
 */
public interface FyRoleMapper extends BaseMapper<FyRole> {

}
