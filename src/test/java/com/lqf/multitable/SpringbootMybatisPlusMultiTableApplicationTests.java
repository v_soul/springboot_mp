package com.lqf.multitable;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lqf.multitable.bean.crm.UserRoleVo;
import com.lqf.multitable.service.crm.FyUserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringbootMybatisPlusMultiTableApplicationTests {

    @Autowired
    private FyUserService service;

    /**
     * 联表查询并分页
     */
    @Test
    public void contextLoads() {
        // 当前页，总条数 构造 page 对象
        Page<UserRoleVo> page = new Page<>(1, 10);
        page.setRecords(service.selectUserListPage(page));
        System.out.println(page);
    }

}
